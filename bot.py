import os
from getpass import getpass
from shlex import join as cmd_join
from matrix_bot_lib import MatrixBot

async def main() -> None:
    BOT_USER = "@dtuhax-bot:xn--sb-lka.org"
    BOT_PASS = getpass(f'Password for {BOT_USER}: ')

    bot = MatrixBot(BOT_USER)
    await bot.login(BOT_PASS)

    @bot.on_message
    async def recv_msg(room_id: str, content: dict, metadata: dict):
        match content:
            case {'body': msg_txt, 'msgtype': 'm.text'}:
                print(f'Message in pizza room: {msg_txt}')
                if msg_txt.lower().startswith("!speak"):
                    escaped_cmd = cmd_join(["timeout", "10", "espeak-ng", "-s", "150", msg_txt[6:]])
                    os.system(f'{escaped_cmd} &')
                if 'door' in msg_txt.lower() or '🚪' in msg_txt:
                    os.system('timeout 2.8 aplay /home/hackerlab/Music/doorbell-1.wav &')

    @bot.on_reaction
    async def recv_react(room_id: str, content: dict, metadata: dict):
        """ MSC2677 """
        match content:
            case {'m.relates_to': {'rel_type': 'm.annotation', 'key': emoji}}:
                if emoji == '🚪':
                    os.system('timeout 2.8 aplay /home/hackerlab/Music/doorbell-1.wav &')

    await bot.run(full_sync=False)

if __name__ == '__main__':
    import asyncio
    asyncio.run(main())
