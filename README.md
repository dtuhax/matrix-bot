# matrix-bot

This is a simple bot that helps DTU students `!speak words & 🍕`.

Requirements:
 * `python >= 3.10`
 * `pip3 install matrix-bot-lib`
   - (It requires: `pip3 install 'httpx[http2] result'`)

## crontab

```
# m h  dom mon dow   command
 29 18 *   *   *     bash -c "sleep 46 && XDG_RUNTIME_DIR=/run/user/1000 cvlc /home/hackerlab/Music/jaws.wav vlc://quit"
 30 18 *   *   *     bash -c "XDG_RUNTIME_DIR=/run/user/1000 cvlc /home/hackerlab/Music/pizza-music.m4a vlc://quit"
 59 23 *   *   *     bash -c "XDG_RUNTIME_DIR=/run/user/1000 cvlc /home/hackerlab/Music/ClosingTime.mp3 vlc://quit"
# dow can be set to 3 for only wed
```
